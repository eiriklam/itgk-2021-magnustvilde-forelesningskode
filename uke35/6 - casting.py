'''Casting'''

# Hent inn tre tall fra bruker. Gjør om de to første til flyttall, og legg de sammen.
# Legg så sammen det siste tallet foran resultatet av de to tallene, slik at tallet blir mye større
tall1 = float(input('Skriv inn et desimaltall: '))
tall2 = float(input('Skriv inn enda et desimaltall: '))
tall3 = input('Skriv inn et desimaltall: ')
resultat = tall1+tall2
print(resultat)
leggeSammen = tall3 + str(resultat)
print(leggeSammen)


# still et ja/Nei spørsmål til bruker der bruker skal skrive 'True' eller 'False', og
# gjør resultatet om til en boolean

svar = bool(input('Heter du Magnus? Svar med \'True\' eller trykk Enter: '))
print(svar)



