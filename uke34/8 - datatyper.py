'''datatyper'''

# string, int, float, boolean
tekst = 'Magnus'
tall = 5
desimal = 4.32
sant = True
usant = False


# legge sammen datatyper og casting
tallTekst = '5'
gjorOm = int(tallTekst)
igjen = float(tallTekst)