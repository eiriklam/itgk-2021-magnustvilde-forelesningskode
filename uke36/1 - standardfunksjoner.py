'''Standardfunksjoner'''
#noen variabel å jobbe med
a, b, c, d, e, f = -2, 4, 5, -1.5, 0, 6
tekst = 'Hva er lengden av denne strengen?'

# finn maksverdi av variablene over (max)

maks = max(a,b,c,d,e,f)
print(f'Maksimum: {maks}')



# finn minimum av variablene over (min)
mini = min(a,b,c,d,e,f)
print(f'Minimum: {mini}')



# finn absoluttverdien av variabelen d (abs)
absolutt = abs(d)
print(f'Absoluttverdi: {absolutt}')



# avrund variabelen d (round)
avrundet = round(d)
print(f'Avrundet: {avrundet}')



# finn lengden av tekst (len)
lengde = len(tekst)
print(f'Lengde: {lengde}')
